$(document).ready(function () {
    //show menu
    $('.navbar-toggler').on('click', function () {
        $(this).parent().toggleClass('active');
        $('.navbar-collapse,.modal-nav').toggleClass('show');
    });
    $('.sub-menu li a,.modal-nav,.nosub').on('click',function (){
        $('.navbar-collapse,.modal-nav').removeClass('show');
        $('.btn-toggle').removeClass('active');
    });
// active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"'],.nav-category li a[href$='"+urlcurrent+"']").addClass('active');


    $(window).on("load", function (e) {
        $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
    });

});
